using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController1 : MonoBehaviour
{
    public GameObject player;
    private controller RR;

    private GameObject cameralookAt, cameraPos;
    
    private float speed = 0;

    public float defaltFOV = 0, desiredFOV = 0;
    [Range(0, 50)] public float smothTime = 8;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        
        cameraPos = player.transform.Find("camera constraint").gameObject;
        cameralookAt = player.transform.Find("camera lookAt").gameObject;

        RR = player.GetComponent<controller>();

        defaltFOV = Camera.main.fieldOfView + 15;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        follow();
        boostFOV();

    }

    private void follow()
    {
        speed = RR.KPH / smothTime;
        gameObject.transform.position = Vector3.Lerp(transform.position, cameraPos.transform.position, Time.deltaTime * speed);
        gameObject.transform.LookAt(cameralookAt.gameObject.transform.position);
    }

    private void boostFOV()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, desiredFOV, Time.deltaTime * 5);
        }
        else
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, defaltFOV, Time.deltaTime * 5);
        }
    }

}
