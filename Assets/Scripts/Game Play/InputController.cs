using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public float vertical;
    public float horizontal;
    public bool handbrake;
    public bool boosting;

    bool checkVertical = false, checkHorizotal = false, isForword = true, isLeft = true;

    private void FixedUpdate()
    {
        if (checkVertical)
        {
            if (isForword && vertical <= 1)
            {
                vertical += Time.deltaTime * 10;
            }
            else if (!isForword && vertical >= -1)
            {
                vertical -= Time.deltaTime * 10;
            }
        }
        else
        {
            vertical = 0;
        }

        if (checkHorizotal)
        {
            if (isLeft && horizontal <= 1)
            {
                horizontal += Time.deltaTime * 10;
            }
            else if (!isLeft && horizontal >= -1)
            {
                horizontal -= Time.deltaTime * 10;
            }
        }
        else
        {
            horizontal = 0;
        }
        //vertical = Input.GetAxis("Vertical"); 
        //horizontal = Input.GetAxis("Horizontal");
        //handbrake = Input.GetAxis("Jump") != 0 ? true : false;
        //boosting = Input.GetKey(KeyCode.LeftShift);
    }

    public void MoveForward()
    {
        checkVertical = true;
        isForword = true;
    }

    public void MoveBackward()
    {
        checkVertical = true;
        isForword = false;
    }

    public void MoveRight()
    {
        checkHorizotal = true;
        isLeft = false;
    }

    public void MoveLeft()
    {
        checkHorizotal = true;
        isLeft = true;
    }

    public void EndMove()
    {
        checkVertical = false;
    }

    public void EndRotate()
    {
        checkHorizotal = false;
    }

}
