using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcadeCar : MonoBehaviour
{

    internal enum driveType
    {
        frontWheelDrive,
        rearWheelDrive,
        allWheelDrive
    }
    [SerializeField]
    private driveType drive;

    internal enum gearBox
    {
        automatic,
        manual
    }
    [SerializeField]
    private gearBox gearChange;

    [SerializeField]
    private GameManager manager;

    [HideInInspector] public bool test; //engine sound boolean

    [Header("Variables")]
    private float totalPower;
    public float maxRPM, minRPM;
    public float[] gears;
    public float[] gearChangeSpeed;
    public AnimationCurve enginePower;

    private InputController inputController;
    private Rigidbody rigidbody;

    public WheelCollider[] wheels = new WheelCollider[4];
    public GameObject[] wheelMeshs = new GameObject[4];
    public GameObject centerOfMass;

    public int gearNum = 1;
    [HideInInspector] public float KPH;
    [HideInInspector] public float engineRPM;
    [HideInInspector] public bool reverse = false;

    //car Shop Values
    public int carPrice;
    public string carName;

    private float smoothTime = 0.09f;

    private float brakePower = 50000;
    private float thrust = 20000f;
    private float downForceValue = 50;
    private float radius = 6, brakPower = 0, horizontal, vertical, lastValue, wheelsRPM;
    private float motorTorque = 100f;
    private float steeringMax = 4;

    public float[] slip = new float[4];

    private bool flag = false;

    // Start is called before the first frame update
    void Start()
    {
        getObjects();
    }

    private void getObjects()
    {
        inputController = GetComponent<InputController>();
        rigidbody = GetComponent<Rigidbody>();
        centerOfMass = GameObject.Find("mass");
        rigidbody.centerOfMass = centerOfMass.transform.localPosition;
    }

    private void FixedUpdate()
    {
        horizontal = inputController.horizontal;
        vertical = inputController.vertical;

        //lastValue = engineRPM;

        addDownForce();

        animateWheels();

        steerVehicle();

        //getFriction();

        calculateEnginePower();
        //if (gameObject.tag == "AI") return;

    }



    private void Update()
    {
        shifter();
    }

    private void calculateEnginePower()
    {

        wheelRPM();

        totalPower = enginePower.Evaluate(engineRPM) * (gears[gearNum]) * vertical;
        float velocity = 0.0f;
        engineRPM = Mathf.SmoothDamp(engineRPM, 1000 + (Mathf.Abs(wheelsRPM) * 3.6f * (gears[gearNum])), ref velocity, smoothTime);

        //if (vertical != 0)
        //{
        //    rigidbody.drag = 0.005f;
        //}
        //if (vertical == 0)
        //{
        //    rigidbody.drag = 0.1f;
        //}
        //totalPower = 3.6f * enginePower.Evaluate(engineRPM) * (vertical);

        //float velocity = 0.0f;
        //if (engineRPM >= maxRPM || flag)
        //{
        //    engineRPM = Mathf.SmoothDamp(engineRPM, maxRPM - 500, ref velocity, 0.05f);

        //    flag = (engineRPM >= maxRPM - 450) ? true : false;
        //    test = (lastValue > engineRPM) ? true : false;
        //}
        //else
        //{
        //    engineRPM = Mathf.SmoothDamp(engineRPM, 1000 + (Mathf.Abs(wheelsRPM) * 3.6f * (gears[gearNum])), ref velocity, smoothTime);
        //    test = false;
        //}
        //if (engineRPM >= maxRPM + 1000) engineRPM = maxRPM + 1000; // clamp at max

        moveVehicle();

        //shifter();
    }

    private void wheelRPM()
    {
        float sum = 0;
        int R = 0;
        for (int i = 0; i < 4; i++)
        {
            sum += wheels[i].rpm;
            R++;
        }
        wheelsRPM = (R != 0) ? sum / R : 0;

        if (wheelsRPM < 0 && !reverse)
        {
            reverse = true;
            manager.changeGear();
        }
        else if (wheelsRPM > 0 && reverse)
        {
            reverse = false;
            manager.changeGear();
        }
    }

    private bool checkGears()
    {
        if (KPH >= gearChangeSpeed[gearNum]) return true;
        else return false;
    }

    private void shifter()
    {
        if (!isGrounded()) return;
        if (gearChange == gearBox.automatic)
        {
            if (engineRPM > maxRPM && gearNum < gears.Length - 1)
            {
                gearNum++;
                manager.changeGear();
            }
            if (engineRPM < minRPM && gearNum > 0)
            {
                gearNum--;
                manager.changeGear();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.J) && gearNum < gears.Length - 1)
            {
                gearNum++;
                manager.changeGear();
            }
            if (Input.GetKeyDown(KeyCode.L) && gearNum > 0)
            {
                gearNum--;
                manager.changeGear();
            }
        }

        
        ////automatic
        //if (engineRPM > maxRPM && gearNum < gears.Length - 1 && !reverse && checkGears())
        //{
        //    gearNum++;
        //    if (gameObject.tag != "AI") manager.changeGear();
        //    return;
        //}
        //if (engineRPM < minRPM && gearNum > 0)
        //{
        //    gearNum--;
        //    if (gameObject.tag != "AI") manager.changeGear();
        //}

    }

    private bool isGrounded()
    {
        if (wheels[0].isGrounded && wheels[1].isGrounded && wheels[2].isGrounded && wheels[3].isGrounded)
            return true;
        else
            return false;
    }

    private void addDownForce()
    {
        rigidbody.AddForce(-transform.up * downForceValue * rigidbody.velocity.magnitude);
    }

    private void steerVehicle()
    {
        if(inputController.horizontal > 0)
        {
            wheels[0].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * inputController.horizontal;
            wheels[1].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * inputController.horizontal;
        } else if (inputController.horizontal < 0)
        {
            wheels[0].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * inputController.horizontal;
            wheels[1].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * inputController.horizontal;
        } else
        {
            wheels[0].steerAngle = 0;
            wheels[1].steerAngle = 0;
        }
        
    }

    private void moveVehicle()
    {
        brakeVehicle();

        if (drive == driveType.allWheelDrive){
            for (int i = 0; i < wheels.Length; i++){
                wheels[i].motorTorque = totalPower / 4;
                wheels[i].brakeTorque = brakPower;
            }
        }else if(drive == driveType.rearWheelDrive){
            wheels[2].motorTorque = totalPower / 2;
            wheels[3].motorTorque = totalPower / 2;

            for (int i = 0; i < wheels.Length; i++)
            {
                wheels[i].brakeTorque = brakPower;
            }
        }
        else{
            wheels[0].motorTorque = totalPower / 2;
            wheels[1].motorTorque = totalPower / 2;

            for (int i = 0; i < wheels.Length; i++)
            {
                wheels[i].brakeTorque = brakPower;
            }
        }

        KPH = rigidbody.velocity.magnitude * 3.6f;

     
    }

    private void brakeVehicle()
    {

        if (vertical < 0)
        {
            brakPower = (KPH >= 10) ? 500 : 0;
        }
        else if (vertical == 0 && (KPH <= 10 || KPH >= -10))
        {
            brakPower = 10;
        }
        else
        {
            brakPower = 0;
        }


    }

    void animateWheels()
    {
        Vector3 pos = transform.position;
        Quaternion quat = transform.rotation;

        for (int i = 0; i < 4; i++)
        {
            wheels[i].GetWorldPose(out pos, out quat);

            wheelMeshs[i].transform.position = pos;
            wheelMeshs[i].transform.rotation = quat;
        }

    }

    private void getFriction()
    {
        for (int i = 0; i < wheels.Length; i++)
        {
            WheelHit wheelHit;
            wheels[i].GetGroundHit(out wheelHit);

            slip[i] = wheelHit.forwardSlip;
        }
    }



}
