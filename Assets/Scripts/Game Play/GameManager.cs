using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public vehicleList list;

    public controller RR;
    public GameObject neeedle;
    public Text kph;
    public Text gearNum;

    public GameObject startPosition;

    public float startPosiziton = -32f, endPosition = 140f;
    private float desiredPosition;

    private void Awake()
    {
        Instantiate(list.vehicles[PlayerPrefs.GetInt("pointer")], startPosition.transform.position, startPosition.transform.rotation);
        RR = GameObject.FindGameObjectWithTag("Player").GetComponent<controller>();
    }

    private void FixedUpdate()
    {
        kph.text = RR.KPH.ToString("0");
        updateNeedle();
    }

    public void updateNeedle()
    {
        desiredPosition = endPosition - startPosiziton;
        float temp = RR.engineRPM / 10000;
        neeedle.transform.eulerAngles = new Vector3(0, 180, (startPosiziton + temp * desiredPosition));

    }

    public void changeGear()
    {
        gearNum.text = (!RR.reverse) ? (RR.gearNum + 1).ToString() : "R";
    }
}
