using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum DayInWeekGame
{
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
}
public enum TypeReward
{
    Gold,
    Item_Speed
}
[System.Serializable]
public class InfoMap
{
    public List<Map> listMap = new List<Map>();
}

[System.Serializable]
public class Map
{
    public string nameMap;
    public double distance;
    public int difficult;
    public bool isWInner;
    public double timer;
    public double reward;
}

[System.Serializable]
public class InfoCar
{
    public List<Car> listCar = new List<Car>();
}

[System.Serializable]
public class Car
{
    public string carName;
    public int carPrice;
    public int topSpeed;
    public int handing;
    public int nitro;
    public bool isPaid;
    public int levelSpeed;
    public int levelHangding;
    public int levelNitro;
}
[System.Serializable]
public class Infogame
{
    public float valBGM;
    public float valSE;
    public double coin;
    public double itemSpeed;
    public List<DailyGift> dailyGifts = new List<DailyGift>();
}
[System.Serializable]
public class DailyGift
{
    public DayInWeekGame day;
    public double quantity;
    public TypeReward typeReward;
    public bool isClaimed;
}
public class DEFINE
{
    public static string SCENE_MENU = "Menu";
    public static string SCENE_GAMEPLAY = "Gameplay";
    public static string SCENE_COMUNITY = "ComunityMap";

    public static string SAVE_MAP = "SAVE_MAP";
    public static string SAVE_CAR = "SAVE_CAR";
    public static string SAVE_GAME = "SAVE_GAME";

    public static string SAVE_WEEK_IN_MONTH = "SAVE_WEEK_IN_MONTH";
}
