using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;
    public InfoMap infoMap;
    public InfoCar infoCar;
    public Infogame infoGame;
    public vehicleList listOfVehicles;


    public void Awake()
    {
        //textDifficultItalia.text = DataManager.Instance.infoMap.listMap[0].difficult.ToString();
        //hoan thanh map::
        //DataManager.Instance.infoMap.listMap[0].isWInner = true;
        //DataManager.Instance.infoMap.listMap[0].timer = 300;
        //DataManager.Instance.SaveInfoMap();

        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        infoMap = new InfoMap();
        infoCar = new InfoCar();
        LoadInfoMap();
        LoadInfoCar();
        LoadInforGame();
    }


    public void SaveInfoMap()
    {
        string jsonMap = JsonUtility.ToJson(infoMap);
        Debug.Log("jsonMap: " + jsonMap);
        PlayerPrefs.SetString(DEFINE.SAVE_MAP, jsonMap);
    }
    public void LoadInfoMap()
    {
        if(!PlayerPrefs.HasKey(DEFINE.SAVE_MAP))
        {
            Map mapItalia = new Map();
            mapItalia.nameMap = "Italia";
            mapItalia.difficult = 3;
            mapItalia.distance = 180;
            mapItalia.isWInner = false;
            mapItalia.timer = 0;
            mapItalia.reward = 1000;

            infoMap.listMap.Add(mapItalia);

            Map mapGermanay = new Map();
            mapGermanay.nameMap = "Germany";
            mapGermanay.difficult = 4;
            mapGermanay.distance = 200;
            mapGermanay.isWInner = false;
            mapGermanay.timer = 0;
            mapGermanay.reward = 1200;

            infoMap.listMap.Add(mapGermanay);

            string jsonMap = JsonUtility.ToJson(infoMap);
            Debug.Log("jsonMap: " + jsonMap);
            PlayerPrefs.SetString(DEFINE.SAVE_MAP, jsonMap);

        }
        else
        {
            string jsonMapLoad = PlayerPrefs.GetString(DEFINE.SAVE_MAP);
            infoMap = JsonUtility.FromJson<InfoMap>(jsonMapLoad);
        }
    }

    public void SaveInfoCar()
    {
        string jsonCar = JsonUtility.ToJson(infoCar);
        Debug.Log("jsonMap: " + jsonCar);
        PlayerPrefs.SetString(DEFINE.SAVE_CAR, jsonCar);
    }

    public void LoadInfoCar()
    {
        if (!PlayerPrefs.HasKey(DEFINE.SAVE_CAR))
        {
            for (int i = 0; i < listOfVehicles.vehicles.Length; i++)
            {
                Car car = new Car();
                car.carName = listOfVehicles.vehicles[i].GetComponent<controller>().carName;
                car.carPrice = listOfVehicles.vehicles[i].GetComponent<controller>().carPrice;
                car.topSpeed = listOfVehicles.vehicles[i].GetComponent<UpgradeController>().topSpeed;
                car.handing = listOfVehicles.vehicles[i].GetComponent<UpgradeController>().handing;
                car.nitro = listOfVehicles.vehicles[i].GetComponent<UpgradeController>().nitro;
                car.isPaid = false;
                car.levelSpeed = 0;
                car.levelHangding = 0;
                car.levelNitro = 0;
                infoCar.listCar.Add(car);
            }

            string jsonCar = JsonUtility.ToJson(infoCar);
            Debug.Log("jsonMap: " + jsonCar);
            PlayerPrefs.SetString(DEFINE.SAVE_CAR, jsonCar);

        }
        else
        {
            string jsonCarLoad = PlayerPrefs.GetString(DEFINE.SAVE_CAR);
            infoCar = JsonUtility.FromJson<InfoCar>(jsonCarLoad);
        }
    }

    public void LoadInforGame()
    {
        if(!PlayerPrefs.HasKey(DEFINE.SAVE_GAME))
        {
            infoGame.valBGM = 1;
            infoGame.valSE = 1;
            DailyGift daily2 = new DailyGift();
            daily2.day = DayInWeekGame.Monday;
            daily2.typeReward = TypeReward.Gold;
            daily2.quantity = 200;
            daily2.isClaimed = true;

            DailyGift daily3 = new DailyGift();
            daily3.day = DayInWeekGame.Tuesday;
            daily3.typeReward = TypeReward.Gold;
            daily3.quantity = 300;
            daily3.isClaimed = true;

            DailyGift daily4 = new DailyGift();
            daily4.day = DayInWeekGame.Wednesday;
            daily4.typeReward = TypeReward.Item_Speed;
            daily4.quantity = 2;
            daily4.isClaimed = false;

            DailyGift daily5 = new DailyGift();
            daily5.day = DayInWeekGame.Thursday;
            daily5.typeReward = TypeReward.Gold;
            daily5.quantity = 500;
            daily5.isClaimed = false;

            DailyGift daily6 = new DailyGift();
            daily6.day = DayInWeekGame.Friday;
            daily6.typeReward = TypeReward.Gold;
            daily6.quantity = 600;
            daily6.isClaimed = false;

            DailyGift daily7 = new DailyGift();
            daily7.day = DayInWeekGame.Saturday;
            daily7.typeReward = TypeReward.Item_Speed;
            daily7.quantity = 3;
            daily7.isClaimed = false;

            DailyGift daily8 = new DailyGift();
            daily8.day = DayInWeekGame.Sunday;
            daily8.typeReward = TypeReward.Gold;
            daily8.quantity = 800;
            daily8.isClaimed = false;

            infoGame.dailyGifts.Add(daily2);
            infoGame.dailyGifts.Add(daily3);
            infoGame.dailyGifts.Add(daily4);
            infoGame.dailyGifts.Add(daily5);
            infoGame.dailyGifts.Add(daily6);
            infoGame.dailyGifts.Add(daily7);
            infoGame.dailyGifts.Add(daily8);

            string jsonGame = JsonUtility.ToJson(infoGame);
            PlayerPrefs.SetString(DEFINE.SAVE_GAME, jsonGame);
        }
        else
        {
            string jsonGameLoad = PlayerPrefs.GetString(DEFINE.SAVE_GAME);
            infoGame = JsonUtility.FromJson<Infogame>(jsonGameLoad);

            
        }
    }
    public bool GetCurrentDailyGift()
    {
        for (int i = 0; i < infoGame.dailyGifts.Count; i++)
        {
            if (infoGame.dailyGifts[i].day.ToString().Equals(DateTime.Now.DayOfWeek.ToString()))
            {
                return infoGame.dailyGifts[i].isClaimed;
            }
        }
        return false;
    } 
    public void SaveClaim(int index)
    {
        infoGame.dailyGifts[index].isClaimed = true;
        SaveInforGame();
    }
    public void ResetClaim()
    {
        for(int i=0; i < infoGame.dailyGifts.Count;i++)
        {
            infoGame.dailyGifts[i].isClaimed = false;
        }
        SaveInforGame();
    }
    public void SaveInforGame()
    {
        string jsonGame = JsonUtility.ToJson(infoGame);
        PlayerPrefs.SetString(DEFINE.SAVE_GAME, jsonGame);
    }
}
