using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    [SerializeField]
    private MapModel[] mapList;

    private void Start()
    {
        List<Map> dataMap = DataManager.Instance.infoMap.listMap;
        for (int i = 0; i < mapList.Length; i++)
        {
            mapList[i].LoadDataMap(dataMap[i].nameMap, dataMap[i].distance, dataMap[i].difficult, dataMap[i].isWInner, dataMap[i].timer, dataMap[i].reward);
        }
    }

}
