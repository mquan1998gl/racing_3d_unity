using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MapModel : MonoBehaviour
{
    public Text nameMap;
    public TextMeshProUGUI distance;
    public GameObject difficult;
    public TextMeshProUGUI complete;
    public TextMeshProUGUI timer;
    public TextMeshProUGUI reward;

    public void LoadDataMap(string nameMapD, double distanceD, int difficultD, bool isWInnerD, double timerD, double rewardD)
    {
        nameMap.text = nameMapD;
        distance.text = distanceD.ToString() + " km";
        timer.text = timerD.ToString();
        reward.text = rewardD.ToString();
        complete.text = isWInnerD ? "100%" : "0%";
        for (int j = 1; j < difficultD; j++)
        {
            Instantiate(difficult.transform.Find("Image").gameObject, difficult.transform);
        }
    }

}
