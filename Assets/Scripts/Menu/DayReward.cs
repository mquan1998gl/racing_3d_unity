using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DayReward : MonoBehaviour
{
    public GameObject Background;
    public GameObject Passed;
    public DayInWeekGame dayInWeekGame;
    public GameObject GoldObj;
    public GameObject SpeedObj;

    public void Lock()
    {
        //nhan roi//
        Background.SetActive(true);
        Passed.SetActive(true);
    }
    public void Normal()
    {
        // > //
        Passed.SetActive(false);
        Background.SetActive(false);
    }
    public void UnLock()
    {
        // current///
        Background.SetActive(true);
        Passed.SetActive(false);
    }
    public void Miss()
    {
        // < not clamin//
        Background.SetActive(false);
        Passed.SetActive(true);
    }
    public void LoadData(TypeReward typeReward , double quantity)
    {
        if (typeReward.Equals(TypeReward.Gold))
        {
            GoldObj.SetActive(true);
            SpeedObj.SetActive(false);
            GoldObj.GetComponent<TextMeshProUGUI>().text = quantity.ToString();
        }
        else
        {
            GoldObj.SetActive(false);
            SpeedObj.SetActive(true);
            SpeedObj.GetComponent<TextMeshProUGUI>().text = quantity.ToString() + " speed";
        }
    }
}
