using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class DailyGiftController : MonoBehaviour
{
    public List<DayReward> dayRewards;
    [SerializeField] private GameObject claimBtn;
    int indexDaily;
    int currentWeek;
    void CheckResetDailyGift()
    {
        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
        DateTime date1 = System.DateTime.Now;
        Calendar cal = dfi.Calendar;
        currentWeek = cal.GetWeekOfYear(date1, dfi.CalendarWeekRule,
                                          dfi.FirstDayOfWeek);
        int weekSave = PlayerPrefs.GetInt(DEFINE.SAVE_WEEK_IN_MONTH);
        Debug.Log("weekSave:  " + weekSave + " ---- current week: " + currentWeek);
        if(currentWeek > weekSave)
        {
            //reset daily gif//
            DataManager.Instance.ResetClaim();
        }
    }
    private void OnEnable()
    {
        CheckResetDailyGift();

        //int day = ((int)DateTime.Now.DayOfWeek == 0) ? 7 : (int)DateTime.Now.DayOfWeek;
        //Debug.Log("dayyy : " + day);
        //cap nhat reward//
        for (int i=0; i < dayRewards.Count;i++)
        {
            dayRewards[i].LoadData(DataManager.Instance.infoGame.dailyGifts[i].typeReward, DataManager.Instance.infoGame.dailyGifts[i].quantity);
            if (DateTime.Now.DayOfWeek.ToString().Equals(dayRewards[i].dayInWeekGame.ToString()))
            {
                //trung//
                if(DataManager.Instance.infoGame.dailyGifts[i].isClaimed)
                {
                    //nhan roi//
                    claimBtn.SetActive(false);
                    dayRewards[i].Lock();
                }
                else
                {
                    //chua nhan//
                    claimBtn.SetActive(true);
                    //Debug.Log("sai o day: " + DateTime.Now.DayOfWeek + " " + dayRewards[i].dayInWeekGame);
                    indexDaily = i;
                    dayRewards[i].UnLock();
                }
            }
            else
            {
                //khong trung < >//

                int dayNow = ((int)DateTime.Now.DayOfWeek == 0) ? 7 : (int)DateTime.Now.DayOfWeek;
                int dayDaily = i + 1;

                //Debug.Log("day Daily: "+ dayNow + " " + dayDaily );
                if(dayDaily < dayNow)
                {
                    if (DataManager.Instance.infoGame.dailyGifts[i].isClaimed)
                    {
                        Debug.Log("debug : " + dayDaily + " " + dayNow + " " + dayRewards[i].dayInWeekGame);
                        //nhan roi//
                        claimBtn.SetActive(false);
                        dayRewards[i].Lock();
                    }
                    else
                    {
                        //chua nhan//
                        claimBtn.SetActive(false);
                        dayRewards[i].Miss();
                    }
                }
                else
                {
                    dayRewards[i].Normal();

                }
            }
        }
    }

    public void ClaimReward()
    {
        Debug.Log("Clame " );
        claimBtn.SetActive(false);
        dayRewards[indexDaily].Lock();
        DataManager.Instance.SaveClaim(indexDaily);
        PlayerPrefs.SetInt(DEFINE.SAVE_WEEK_IN_MONTH, currentWeek);
    }
}
