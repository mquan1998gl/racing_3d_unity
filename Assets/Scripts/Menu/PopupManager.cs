using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    [SerializeField] private GameObject DailyGift;

    private void Start()
    {
        if (!DataManager.Instance.GetCurrentDailyGift())
        {
            ShowDailyGift();
        }
    }

    public void ShowDailyGift()
    {
        DailyGift.SetActive(true);
    }

    public void CloseDailyGift()
    {
        DailyGift.SetActive(false);
    }
}
