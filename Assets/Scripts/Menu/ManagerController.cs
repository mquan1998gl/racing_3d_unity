using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class ManagerController : MonoBehaviour
{
    [Header("Camera")]
    public float lerpTime;
    public GameObject CameraObject;
    public GameObject finalCameraPosition, startCameraPosition;

    [Header("Deafault Canvas")]
    public GameObject DeafaultCanvas;
    public Text DeafaultCanvasCurrency;

    [Header("Map Canvas")]
    public GameObject mapSelectorCanvas;

    [Header("Shop Canvas")]
    public GameObject shopCanvas;

    [Header("Vehicle Select Canvas")]
    public GameObject vehicleSelectCanvas;
    public GameObject buyButton;
    public GameObject selectedButton;
    public GameObject selectButton;
    public vehicleList listOfVehicles;
    public Text price;
    public Text carName;

    [Header("Upgrade Canvas")]
    public GameObject upgradeCanvas;
    public GameObject speedObj;
    public GameObject handingObj;
    public GameObject nitroObj;
    public TextMeshProUGUI topSpeed;
    public TextMeshProUGUI handing;
    public TextMeshProUGUI nitro;
    public Text speedCoin;
    public Text handingCoin;
    public Text nitroCoin;

    public GameObject toRotate;
    [HideInInspector] public float rotateSpeed = 10f;
    [HideInInspector] public int vehiclePointer = 0;
    private bool finalToStart, startToFinal;

    private void Awake()
    {
        PlayerPrefs.SetInt("pointer", vehiclePointer);
        mapSelectorCanvas.SetActive(false);
        DeafaultCanvas.SetActive(true);
        vehicleSelectCanvas.SetActive(false);
        shopCanvas.SetActive(false);

        DeafaultCanvasCurrency.text = PlayerPrefs.GetInt("currency").ToString();


        vehiclePointer = PlayerPrefs.GetInt("pointer");
        GameObject childObject = Instantiate(listOfVehicles.vehicles[vehiclePointer], Vector3.zero, toRotate.transform.rotation) as GameObject;
        childObject.transform.parent = toRotate.transform;
        getCarInfo();
  
    }

    private void FixedUpdate()
    {
        toRotate.transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
        cameraTranzition();
    }

    public void rightButton()
    {
        if (vehiclePointer < listOfVehicles.vehicles.Length - 1)
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
            vehiclePointer++;
            GameObject childObject = (GameObject)Instantiate(listOfVehicles.vehicles[vehiclePointer], Vector3.zero, toRotate.transform.rotation);
            childObject.transform.parent = toRotate.transform;
            getCarInfo();
        }
    }

    public void leftButton()
    {
        if (vehiclePointer > 0)
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
            vehiclePointer--;
            GameObject childObject = (GameObject)Instantiate(listOfVehicles.vehicles[vehiclePointer], Vector3.zero, toRotate.transform.rotation);
            childObject.transform.parent = toRotate.transform;
            getCarInfo();
        }
    }

    public void backHomeButton()
    {
        Debug.Log("home");
        DeafaultCanvas.SetActive(true);
        vehicleSelectCanvas.SetActive(false);
        mapSelectorCanvas.SetActive(false);
        shopCanvas.SetActive(false);
        startToFinal = false;
        finalToStart = true;
    }

    public void mapSelectorCanvasButton()
    {
        DeafaultCanvas.SetActive(false);
        vehicleSelectCanvas.SetActive(false);
        mapSelectorCanvas.SetActive(true);

    }

    public void BuyButton()
    {
        if (PlayerPrefs.GetInt("currency") >= listOfVehicles.vehicles[vehiclePointer].GetComponent<controller>().carPrice)
        {
            PlayerPrefs.SetInt("currency", PlayerPrefs.GetInt("currency") - listOfVehicles.vehicles[vehiclePointer].GetComponent<controller>().carPrice);

            DataManager.Instance.infoCar.listCar[vehiclePointer].isPaid = true;

            DataManager.Instance.SaveInfoCar();
            
            getCarInfo();
        }

    }

    public void SelectCarButton()
    {
        PlayerPrefs.SetInt("pointer", vehiclePointer);
        getCarInfo();

    }

    public void getCarInfo()
    {
        carName.text = DataManager.Instance.infoCar.listCar[vehiclePointer].carName;
        topSpeed.text = DataManager.Instance.infoCar.listCar[vehiclePointer].topSpeed.ToString();
        handing.text = DataManager.Instance.infoCar.listCar[vehiclePointer].handing.ToString();
        nitro.text = DataManager.Instance.infoCar.listCar[vehiclePointer].nitro.ToString();

        getInfoUpgradeCar();

        DeafaultCanvasCurrency.text = PlayerPrefs.GetInt("currency").ToString("");
        upgradeCanvas.SetActive(false);
        if (DataManager.Instance.infoCar.listCar[vehiclePointer].isPaid)
        {
            upgradeCanvas.SetActive(true);
            if (vehiclePointer == PlayerPrefs.GetInt("pointer"))
            {
                selectButton.SetActive(false);
                selectedButton.SetActive(true);
                buyButton.SetActive(false);
                return;
            }
            selectButton.SetActive(true);
            selectedButton.SetActive(false);
            buyButton.SetActive(false);
            return;
        }
        
        price.text = DataManager.Instance.infoCar.listCar[vehiclePointer].carPrice.ToString();

        selectButton.SetActive(false);
        selectedButton.SetActive(false);
        buyButton.SetActive(buyButton);
        DataManager.Instance.SaveInfoCar();

    }

    public void getInfoUpgradeCar()
    {
        if (DataManager.Instance.infoCar.listCar[vehiclePointer].levelSpeed >= listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade.Length)
        {
            speedObj.SetActive(false);
        }
        else
        {
            speedCoin.text = listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelSpeed].ToString();
            speedObj.SetActive(true);
        }

        if (DataManager.Instance.infoCar.listCar[vehiclePointer].levelHangding >= listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade.Length)
        {
            handingObj.SetActive(false);
        }
        else
        {
            handingCoin.text = listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelHangding].ToString();
            handingObj.SetActive(true);
        }

        if (DataManager.Instance.infoCar.listCar[vehiclePointer].levelNitro >= listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade.Length)
        {
            nitroObj.SetActive(false);
        }
        else
        {
            nitroCoin.text = listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelNitro].ToString();
            nitroObj.SetActive(true);
        }
    }

    public void upgradeSpeed()
    {
        if (PlayerPrefs.GetInt("currency") >= listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelSpeed])
        {
            PlayerPrefs.SetInt("currency", PlayerPrefs.GetInt("currency") -
                listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelSpeed]);
        }
        DataManager.Instance.infoCar.listCar[vehiclePointer].levelSpeed++;
        DataManager.Instance.infoCar.listCar[vehiclePointer].topSpeed += 5 * DataManager.Instance.infoCar.listCar[vehiclePointer].levelSpeed;
        
        getCarInfo();
    }

    public void upgradeHanding()
    {
        if (PlayerPrefs.GetInt("currency") >= listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelHangding])
        {
            PlayerPrefs.SetInt("currency", PlayerPrefs.GetInt("currency") -
                listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelHangding]);
        }
        DataManager.Instance.infoCar.listCar[vehiclePointer].levelHangding++;
        DataManager.Instance.infoCar.listCar[vehiclePointer].handing += 100 * DataManager.Instance.infoCar.listCar[vehiclePointer].levelHangding;
        
        getCarInfo();
    }

    public void upgradeNitro()
    {
        if (PlayerPrefs.GetInt("currency") >= listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelNitro])
        {
            PlayerPrefs.SetInt("currency", PlayerPrefs.GetInt("currency") -
                listOfVehicles.vehicles[vehiclePointer].GetComponent<UpgradeController>().coinUpgrade[DataManager.Instance.infoCar.listCar[vehiclePointer].levelNitro]);
        }
        DataManager.Instance.infoCar.listCar[vehiclePointer].levelNitro++;
        DataManager.Instance.infoCar.listCar[vehiclePointer].nitro += 1 * DataManager.Instance.infoCar.listCar[vehiclePointer].levelNitro;
        
        getCarInfo();
    }

    public void DeafaultCanvasStartButton()
    {
        mapSelectorCanvas.SetActive(false);
        DeafaultCanvas.SetActive(false);
        vehicleSelectCanvas.SetActive(true);
        shopCanvas.SetActive(false);
        
    }

    public void vehicleSelectCanvasStartButton()
    {
        mapSelectorCanvas.SetActive(false);
        DeafaultCanvas.SetActive(false);
        vehicleSelectCanvas.SetActive(true);
        finalToStart = false;
        startToFinal = true;

    }

    public void shopCanvasButton()
    {
        shopCanvas.SetActive(true);
        DeafaultCanvas.SetActive(false);
        vehicleSelectCanvas.SetActive(false);

    }

    public void cameraTranzition()
    {
        if (startToFinal)
        {
            CameraObject.transform.position = Vector3.Lerp(CameraObject.transform.position, finalCameraPosition.transform.position, lerpTime * Time.deltaTime);
        }
        if (finalToStart)
        {
            CameraObject.transform.position = Vector3.Lerp(CameraObject.transform.position, startCameraPosition.transform.position, lerpTime * Time.deltaTime);
        }

    }

    public void loadMarioMap()
    {
        SceneManager.LoadScene(DEFINE.SCENE_GAMEPLAY);
    }

    public void loadComunityMap()
    {
        SceneManager.LoadScene(DEFINE.SCENE_COMUNITY);
    }
}
