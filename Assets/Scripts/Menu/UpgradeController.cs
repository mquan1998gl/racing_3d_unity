using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeController : MonoBehaviour
{
    public int topSpeed;
    public int handing;
    public int nitro;

    public int[] speedUpgrade = new int[4];
    public int[] handingUpgrade = new int[4];
    public int[] nitroUpgrade = new int[4];

    public int[] coinUpgrade = new int[5];

}
